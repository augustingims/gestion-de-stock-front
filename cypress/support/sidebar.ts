export const sidebarSelector =  '[data-cy="sidebar"]';
export const articleSidebarDropDownSelector =  '[data-cy="sideBarDropDown2"]';
export const articleSidebarItemDropDownSelector =  '[data-cy="sideBarItemDropDown21"]';

Cypress.Commands.add('clickOnArticleItem', () => {
    return cy.get(sidebarSelector).get(articleSidebarDropDownSelector).click().get(articleSidebarItemDropDownSelector).click();
});

declare global {
    namespace Cypress {
        interface Chainable {
            clickOnArticleItem(): Cypress.Chainable;
        }
    }
}