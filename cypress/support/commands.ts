/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

// login page
export const usernameLoginSetector =  '[data-cy="loginUsername"]';
export const passwordLoginSetector =  '[data-cy="loginPassword"]';
export const rememberLoginSetector =  '[data-cy="loginRemember"]';
export const submitLoginSetector =  '[data-cy="loginSubmit"]';
export const registerLoginSetector =  '[data-cy="loginRegister"]';
export const forgetPasswordLoginSetector =  '[data-cy="loginForgetPassword"]';
export const errorMessageLoginSetector =  '[data-cy="loginErrorMessage"]';
export const titleLoginSetector =  '[data-cy="loginTitle"]';
export const pageLoginSetector =  '[data-cy="loginPage"]';


Cypress.Commands.add('authenticatedRequest', (data: any) => {
    const bearerToken = sessionStorage.getItem(Cypress.env('jwtTokenName'));
    return cy.request({
        ...data,
        auth: {
            bearer: bearerToken,
        }
    })
});

Cypress.Commands.add('login', (login: string, password: string) => {
    cy.session([login, password], () => {
        cy.request({
            method: 'GET',
            url: 'http://localhost:8081/api/v1/utilisateurs/account',
            failOnStatusCode: false,
        });
        cy.authenticatedRequest({
            method: 'POST',
            body: {login, password},
            url: Cypress.env('authenticationUrl')
        }).then (({body: {accessToken}}) => {
            sessionStorage.setItem('access_token', accessToken);
            localStorage.setItem('access_token', accessToken);
        });
    },{
        validate() {
            cy.authenticatedRequest({url: 'http://localhost:8081/api/v1/utilisateurs/account'}).its('status').should('eq', 200);
        },
    })
});



declare global {
    namespace Cypress {
        interface Chainable {
            login(login: string, password: string): Cypress.Chainable;
            authenticatedRequest(data: any): Cypress.Chainable;
        }
    }
}