import { forgetPasswordLoginSetector, passwordLoginSetector, registerLoginSetector, submitLoginSetector, titleLoginSetector, usernameLoginSetector } from "cypress/support/commands";

describe('Login', () => {

    const username = Cypress.env('E2E_USERNAME') ?? 'gims@gmail.com';
    const password = Cypress.env('E2E_PASSWORD') ?? '@dmin';

    beforeEach(() => {
        cy.visit('/login');
    });

    it('devrait bien s\'afficher', () => {
        cy.get(titleLoginSetector).should('be.visible')
        cy.get(usernameLoginSetector).should('be.visible')
        cy.get(passwordLoginSetector).should('be.visible')
        cy.get(submitLoginSetector).should('be.visible')
        cy.get(registerLoginSetector).should('be.visible')
        cy.get(forgetPasswordLoginSetector).should('be.visible')
        cy.get(submitLoginSetector).should('be.disabled')
    });

    it('devrait afficher un message d\'erreur lorsque l\'email n\'a ete trouve', () => {
        cy.get(usernameLoginSetector).type("g@gmail.com{enter}")
        cy.get(passwordLoginSetector).type(`${password}{enter}`)
        cy.get(submitLoginSetector).click()
        cy.contains('Aucun utilisateur avec cette email').should('be.visible')
    });

    it('devrait afficher un message d\'erreur lorsque le mot de passe est incorrect', () => {
        cy.get(usernameLoginSetector).type(`${username}{enter}`)
        cy.get(passwordLoginSetector).type(`bad-password{enter}`)
        cy.get(submitLoginSetector).click()
        cy.contains('Bad credentials').should('be.visible')
    });

    it('devrait afficher le tableau de bord lorsque la connexion a reussie', () => {
        cy.get(usernameLoginSetector).type(`${username}{enter}`)
        cy.get(passwordLoginSetector).type(`${password}{enter}`)
        cy.get(submitLoginSetector).click()
        cy.url().should('contain','/')
    });



})