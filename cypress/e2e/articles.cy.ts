
describe('Gestion des articles', () => {

    const username = Cypress.env('E2E_USERNAME') ?? 'gims@gmail.com';
    const password = Cypress.env('E2E_PASSWORD') ?? '@dmin';

    beforeEach(() => {
        cy.login(username, password);
        cy.intercept("/api/v1/utilisateurs/account",  { fixture: "/user/user-data.json"}).as('getAccount');
        cy.intercept("/api/v1/articles",  {fixture: "/article/article-data-list.json"}).as('getAllArticles');
        cy.visit('/');
        cy.clickOnArticleItem();
    });

    it('devrait mettre l\'url de article', () => {
        cy.url().should('contain', '/articles');
    });

})