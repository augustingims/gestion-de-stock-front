import { defineConfig } from "cypress";

export default defineConfig({
  fixturesFolder: 'cypress/fixtures',
  viewportHeight: 720,
  viewportWidth: 1280,
  e2e: {
    setupNodeEvents(on, config) {},
    supportFile: 'cypress/support/e2e.ts',
    baseUrl: 'http://localhost:4200',
    specPattern: 'cypress/e2e/**/*.cy.ts',
    env: {
      "authenticationUrl": "http://localhost:8081/api/v1/authenticate",
      "jwtTokenName": "access_token"
    }
  },
});
