pipeline {
    agent any

    environment {
        NODE_TOOLS = 'nodejs_20.14.0'
    }

    tools {
         dockerTool 'docker'
    }

    stages {

        stage('Git Source checkout') {
            steps {
                checkout scm
            }
        }

        stage('Dependency app') {
            steps {
                script {
                    nodejs('nodejs_20.14.0') {
                        sh label: 'npm instal', script: 'npm install'
                    }
                }
            }
        }

        stage('Build app') {
            steps {
                script {
                    nodejs('nodejs_20.14.0') {
                        sh label: 'npm instal', script: 'npm run build:ssr && npm run gzipper'
                    }
                }
            }
        }

        stage('Build image') {
            when {
                branch 'main'
            }
            steps {
                script {
                    def PROJECT = readJSON file: 'package.json'

                    withDockerRegistry(credentialsId: "gitlab-registry", toolName: "docker", url: "https://registry.gitlab.com") {
                        sh label: 'build iamge', script: "docker build -t registry.gitlab.com/augustingims/${PROJECT.name}:${PROJECT.version} -f cicd/Dockerfile ."
                        sh label: 'push image to registry', script: "docker push registry.gitlab.com/augustingims/${PROJECT.name}:${PROJECT.version}"
                        sh label: 'tag image', script: "docker tag registry.gitlab.com/augustingims/${PROJECT.name}:${PROJECT.version} registry.gitlab.com/augustingims/${PROJECT.name}:latest"
                        sh label: 'push image latest to registry', script: "docker push registry.gitlab.com/augustingims/${PROJECT.name}:latest"
                        sh label: 'remove old image', script: "docker image rm registry.gitlab.com/augustingims/${PROJECT.name}:${PROJECT.version}"
                    }
                }
            }
        }

         stage('Deploy App') {
            when {
                branch 'main'
            }
            steps {
                script {
                     withDockerRegistry(credentialsId: "gitlab-registry", toolName: "docker", url: "https://registry.gitlab.com") {
                        sh 'docker stack deploy --with-registry-auth --compose-file cicd/gestion-de-stock.yml devops'
                     }
                }
            }
        }
    }
    post {
       always {
         cleanWs()
       }
    }
}
