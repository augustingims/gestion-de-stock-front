import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/core/auth/account.service';
import { AuthJwtService } from 'src/app/core/auth/auth-jwt.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    private authJwtService: AuthJwtService,
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
  }

  logout(): void {
    this.authJwtService.logout().subscribe({complete: () => {
      this.accountService.authentication(null);
      this.router.navigate(['login']);
    }})
  }

}
