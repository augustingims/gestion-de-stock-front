import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { menuProperties } from '../../shared-member/constants/menu-properties';
import { AccountService } from 'src/app/core/auth/account.service';
import { Utilisateur } from '../../models/utilisateur';
import { Subscription } from 'rxjs';

declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy, AfterViewInit {

  menus = menuProperties;

  account: Utilisateur | null = {};
  authSubscription: Subscription = new Subscription();
  
  constructor(
    private accountService: AccountService
  ) { }

  ngAfterViewInit(): void {
   $('[data-widget=treeview]').Treeview('init');
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.accountService.identify().subscribe();
    this.authSubscription.add(this.accountService.getAuthenticationState().subscribe(account => (this.account = account)));
  }

}
