export interface Sidebar {
    id?: string;
    title?: string;
    icon?: string;
    url?: string;
    active?: boolean;
    subMenu?: Array<Sidebar>;
}
