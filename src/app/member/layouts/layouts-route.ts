import { Routes } from "@angular/router";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { SidebarComponent } from "./sidebar/sidebar.component";

export const layoutsRoutes: Routes = [
    {
        path: '',
        component: HeaderComponent,
        outlet: 'header'

    },
    {
        path: '',
        component: FooterComponent,
        outlet: 'footer'

    },
    {
        path: '',
        component: SidebarComponent,
        outlet: 'sidebar'

    }
]