import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { layoutsRoutes } from './layouts/layouts-route';

const routes: Routes = [
  {
    path: '',
    data: {
      seo : {
        title: "Tableau de bord | Gestion Stock",
        description: "Page Tableau de bord de Gestion de Stock",
      }
    },
    loadChildren: () => import("./pages/dashboard/dashboard.module").then(m => m.DashboardModule)
  },
  {
    path: 'categories',
    loadChildren: () => import("./pages/categories/categories.module").then(m => m.CategoriesModule)
  },
  {
    path: 'articles',
    data: {
      seo : {
        title: "Gestion Article | Gestion Stock",
        description: "Page de gestion des articles",
      }
    },
    loadChildren: () => import("./pages/articles/articles.module").then(m => m.ArticlesModule)
  },
  ...layoutsRoutes
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
