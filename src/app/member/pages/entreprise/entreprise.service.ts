import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Entreprise } from '../../models/entreprise';
import { ApplicationConfigService } from 'src/app/core/config/application-config.service';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {

  protected resourceUrl = "api/v1/entreprises";

  constructor(
    private httpClient: HttpClient,
    private applicationConfigService: ApplicationConfigService
  ) { }

  createEntreprise(entreprise: Entreprise): Observable<HttpResponse<Entreprise>> {
    return this.httpClient.post(this.applicationConfigService.getEndpointFor(this.resourceUrl), entreprise, {observe: 'response'});
  }
}
