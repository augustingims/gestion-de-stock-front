import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesNewUpdateComponent } from './articles-new-update.component';

describe('ArticlesNewUpdateComponent', () => {
  let component: ArticlesNewUpdateComponent;
  let fixture: ComponentFixture<ArticlesNewUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesNewUpdateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArticlesNewUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
