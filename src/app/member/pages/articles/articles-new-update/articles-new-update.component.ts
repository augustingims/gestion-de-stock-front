import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { AccountService } from 'src/app/core/auth/account.service';
import { Article } from 'src/app/member/models/article';
import { Categorie } from 'src/app/member/models/categorie';
import { Utilisateur } from 'src/app/member/models/utilisateur';
import { CategoriesService } from '../../categories/service/categories.service';
import { ArticleService } from '../service/article.service';
import { Photo } from 'src/app/member/models/photo';
import { ToastrService } from 'ngx-toastr';
import { JsonLdService, SeoSocialShareService } from 'ngx-seo';

@Component({
  selector: 'app-articles-new-update',
  templateUrl: './articles-new-update.component.html',
  styleUrls: ['./articles-new-update.component.scss']
})
export class ArticlesNewUpdateComponent implements OnInit, OnDestroy{

  account: Utilisateur | null = {};
  authSubscription = new Subscription();

  article: Article = {};

  categories$: Observable<Categorie[]| []> | undefined;
  errorMsg: Array<string> = [];

  file: File | null | undefined;
  urlImg: string | ArrayBuffer | undefined = 'assets/default-product.jpeg';

  isSaving = false;

  articleForm = this.fb.group({
    codeArticle: [''],
    designation: [''],
    prixUnitaireHt: [0],
    prixUnitaireTtc: [0],
    tauxTva: [0],
    categorie: [0]
  });

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private articleService: ArticleService,
    private accountService: AccountService,
    private categorieService: CategoriesService,
    private activateRoute: ActivatedRoute,
    private seoSocialShareService: SeoSocialShareService,
    private jsonLdService: JsonLdService
  ) { }

  ngOnInit(): void {
    this.authSubscription.add(this.accountService.getAuthenticationState().subscribe(account => (this.account = account)));
    this.categories$ = this.categorieService.findAll();
    this.activateRoute.data.subscribe(({ article }) => {
      this.article = article;
      this.updateForm(article);
      this.articleSeo(article);
    });
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

  previousPage(): void {
    window.history.back();
  }

  calculerTTC(): void {
    const prixUnitaireHt = this.articleForm.get('prixUnitaireHt')?.value ?? 0;
    const tauxTva = this.articleForm.get('tauxTva')?.value ?? 0;

    if(prixUnitaireHt && tauxTva){
      const prixUnitaireTtc = +prixUnitaireHt + (+(prixUnitaireHt * (tauxTva / 100)));
      this.articleForm.patchValue({
        prixUnitaireTtc: prixUnitaireTtc
      })
    }
  }
  
  create(): void {
    this.isSaving = true;
    const article = this.createForm();
    this.articleService.createArticle(article).subscribe({next: res => {
      this.isSaving = false;
      this.savePhoto(res.id, res.codeArticle);
    }, error: error => {
      if(error.error != null && error.error.errors !== undefined ){
        this.errorMsg = error.error.errors;
      }
      this.isSaving = false;
    }})
  }

  onFileInput(files: FileList | null): void {
    if(files){
      this.file = files.item(0);
      if(this.file){
        const fileReader = new FileReader();
        fileReader.readAsDataURL(this.file);
        fileReader.onload = () => {
          if(fileReader.result){
            this.urlImg = fileReader.result;
          }
        }
      }
    }
  }

  savePhoto(id: number | undefined, codeArticle: string | undefined) {
    if(id && codeArticle && this.file){
     const params: Photo = {
       id: id,
       file: this.file,
       title: codeArticle,
       context: 'article'
     }
     this.articleService.savePicture(params).subscribe({next: ()=> {
       this.previousPage();
     }, error: err => {
      if(err.error.message){
        this.toastr.error(err.error.message);
      }
     }})
    } else {
     this.previousPage();
    }
 
   }

  updateForm(article: Article): void {
    this.articleForm.patchValue({
      codeArticle: article?.codeArticle,
      designation: article?.designation,
      prixUnitaireHt: article?.prixUnitaireHt,
      prixUnitaireTtc: article?.prixUnitaireTtc,
      tauxTva: article?.tauxTva,
      categorie: article?.categorie?.id
    });
    this.urlImg = article?.photo ?? 'assets/default-product.jpeg';
  }

  createForm(): Article {
    return {
      ...this.article,
      codeArticle: this.articleForm.get('codeArticle')?.value ?? '',
      designation: this.articleForm.get('designation')?.value ?? '',
      prixUnitaireHt: Number(this.articleForm.get('prixUnitaireHt')?.value) ?? 0,
      prixUnitaireTtc: Number(this.articleForm.get('prixUnitaireTtc')?.value) ?? 0,
      tauxTva: Number(this.articleForm.get('tauxTva')?.value) ?? 0,
      idEntreprise: this.account?.entreprise?.id,
      categorie: {
        id: Number(this.articleForm.get('categorie')?.value) ?? null
      }
    }
  }

  articleSeo(article: Article): void {
    if(article && article.codeArticle != null){
      this.seoSocialShareService.setData({
        title: `${article.codeArticle} - Gestion de stock`,
        description: `${article.designation}`,
        published: new Date().toDateString(),
        image: article.photo,
        author: 'Gestion de stock',
        type: 'website'
      });

      const jsonLdObject = this.jsonLdService.getObject('Website', {
        title: `${article.codeArticle} - Gestion de stock`,
        description: article.designation ?? '',
        datePosted: new Date().toDateString(),
        category: article.categorie?.designation ?? '',
      });
      this.jsonLdService.setData(jsonLdObject);
    }
  }

}
