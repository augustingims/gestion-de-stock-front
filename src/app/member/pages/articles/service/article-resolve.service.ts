import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, map, mergeMap, of, EMPTY } from 'rxjs';
import { Article } from 'src/app/member/models/article';
import { ArticleService } from './article.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleResolveService implements Resolve<Article>{

  constructor(
    private articleService: ArticleService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Article | Observable<Article> | Promise<Article> {
    const id = route.params['id'];
    if(id){
      return this.articleService.findAll({'id': id}).pipe(map(articles => articles[0]), mergeMap((article: Article) => {
        if(article){
          return of(article);
        } else {
          return EMPTY;
        }
      }));
    }
    return of({});
  }
}