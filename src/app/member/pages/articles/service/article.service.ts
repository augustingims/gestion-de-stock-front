import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApplicationConfigService } from 'src/app/core/config/application-config.service';
import { Article } from '../../../models/article';
import { Observable } from 'rxjs';
import { Photo } from 'src/app/member/models/photo';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  protected resourceUrl = "api/v1/articles";
  protected resourceUrlPicture = "api/v1/photos";

  constructor(
    private httpClient: HttpClient,
    private applicationConfigService: ApplicationConfigService
  ) { }

  createArticle(article: Article): Observable<Article> {
    return this.httpClient.post(this.applicationConfigService.getEndpointFor(this.resourceUrl), article);
  }

  findAll(req?: any): Observable<Article[]> {
    const options = this.createRequestParam(req);
    return this.httpClient.get<Article[]>(this.applicationConfigService.getEndpointFor(this.resourceUrl), {params: options});
  }

  savePicture(photo: Photo, req?: any): Observable<Article> {
    const options = this.createRequestParam(req);
    let __body: any = null;
    let __formData = new FormData();
    __body = __formData;
    if (photo.file != null) { __formData.append('file', photo.file as string | Blob);}
    return this.httpClient.post(this.applicationConfigService.getEndpointFor(`${this.resourceUrlPicture}/${photo.id}/item/${photo.title}/name/${photo.context}`),__body, {params: options});
  }

  createRequestParam(req?: any): HttpParams {
    let options: HttpParams = new HttpParams();
    if(req){
      Object.keys(req).forEach(key => {
        options = options.set(key, req[key]);
      })
    }
    return options;
  }

  deleteArticle(articleId: number): Observable<any> {
    return this.httpClient.delete(`${this.applicationConfigService.getEndpointFor(this.resourceUrl)}/${articleId}`);
  }

}
