import { TestBed } from '@angular/core/testing';

import { ArticleResolveService } from './article-resolve.service';

describe('ArticleResolveService', () => {
  let service: ArticleResolveService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArticleResolveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
