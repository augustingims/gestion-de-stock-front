import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesNewUpdateComponent } from './articles-new-update/articles-new-update.component';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticleResolveService } from './service/article-resolve.service';

const routes: Routes = [
  {
    path: '',
    component: ArticlesListComponent
  },
  {
    path: 'new',
    component: ArticlesNewUpdateComponent
  },
  {
    path: ':id/edit',
    component: ArticlesNewUpdateComponent,
    resolve: {
      article: ArticleResolveService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
