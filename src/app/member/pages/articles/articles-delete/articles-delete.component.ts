import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Article } from 'src/app/member/models/article';
import { ArticleService } from '../service/article.service';

@Component({
  selector: 'app-articles-delete',
  templateUrl: './articles-delete.component.html',
  styleUrls: ['./articles-delete.component.scss']
})
export class ArticlesDeleteComponent {

  article: Article = {};
  isVisible = false;
  errorMsg: string = '';

  constructor(
    private articleService: ArticleService,
    private activeModal: NgbActiveModal
  ) { }

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirm(id: number){
    this.isVisible = true;
    this.articleService.deleteArticle(id).subscribe({next: ()=> {
      this.activeModal.close('success');
      this.isVisible = false;
    }, error: err => {
      this.errorMsg = err.error.error;
      this.isVisible = false;
    }})
  }

}
