import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesDeleteComponent } from './articles-delete.component';

describe('ArticlesDeleteComponent', () => {
  let component: ArticlesDeleteComponent;
  let fixture: ComponentFixture<ArticlesDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesDeleteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArticlesDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
