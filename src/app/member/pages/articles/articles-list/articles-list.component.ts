import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Article } from 'src/app/member/models/article';
import { ArticleService } from '../service/article.service';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.scss']
})
export class ArticlesListComponent implements OnInit {

  articles$: Observable<Article[] | []> | undefined; 

  constructor(
    private router: Router,
    private articleService: ArticleService
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  addArticle(): void {
    this.router.navigate(['articles/new']);
  }

  refresh(): void {
    this.articles$ = this.articleService.findAll();
  }

  trackBy(_index: number, article: Article): number{
    return article.id!;
  }

  handleDelete(event: any){
    if(event === 'article-delete'){
      this.refresh();
    }
  }

}
