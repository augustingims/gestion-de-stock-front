import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticlesNewUpdateComponent } from './articles-new-update/articles-new-update.component';
import { ArticlesDeleteComponent } from './articles-delete/articles-delete.component';
import { SharedMemberModule } from '../../shared-member/shared-member.module';
import { ArticleItemComponent } from '../../components/article-item/article-item.component';


@NgModule({
  declarations: [
    ArticlesListComponent,
    ArticlesNewUpdateComponent,
    ArticlesDeleteComponent,
    ArticleItemComponent
  ],
  imports: [
    SharedMemberModule,
    ArticlesRoutingModule
  ],
  entryComponents: [
    ArticlesDeleteComponent
  ]
})
export class ArticlesModule { }
