import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedMemberModule } from '../../shared-member/shared-member.module';


@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    SharedMemberModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
