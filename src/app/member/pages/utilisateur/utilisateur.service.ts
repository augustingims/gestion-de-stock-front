import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Utilisateur } from '../../models/utilisateur';
import { ChangerMotDePasseUtilisateur } from 'src/app/public/models/changer-mot-de-passe-utilisateur';
import { ApplicationConfigService } from 'src/app/core/config/application-config.service';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  protected resourceUrl = "api/v1/utilisateurs";

  constructor(
    private httpClient: HttpClient,
    private applicationConfigService: ApplicationConfigService
  ) { }

  currentUser(): Observable<Utilisateur> {
    return this.httpClient.get<Utilisateur>(this.applicationConfigService.getEndpointFor(`${this.resourceUrl}/account`));
  }

  changePassword(userId: number, changePassword: ChangerMotDePasseUtilisateur): Observable<Utilisateur> {
    return this.httpClient.put(this.applicationConfigService.getEndpointFor(`${this.resourceUrl}/${userId}/password`), changePassword);
  }
}
