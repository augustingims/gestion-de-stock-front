import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApplicationConfigService } from 'src/app/core/config/application-config.service';
import { Categorie } from 'src/app/member/models/categorie';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  protected resourceUrl = "api/v1/categories";

  constructor(
    private httpClient: HttpClient,
    private applicationConfigService: ApplicationConfigService
  ) { }

  createCategory(categorie: Categorie): Observable<Categorie> {
    return this.httpClient.post(this.applicationConfigService.getEndpointFor(this.resourceUrl), categorie);
  }

  findAll(req?: any): Observable<Categorie[]> {
    const options = this.createRequestParam(req);
    return this.httpClient.get<Categorie[]>(this.applicationConfigService.getEndpointFor(this.resourceUrl), {params: options});
  }

  createRequestParam(req?: any): HttpParams {
    let options: HttpParams = new HttpParams();
    if(req){
      Object.keys(req).forEach(key => {
        options = options.set(key, req[key]);
      })
    }
    return options;
  }

  deleteCategories(categoryId: number): Observable<any> {
    return this.httpClient.delete(`${this.applicationConfigService.getEndpointFor(this.resourceUrl)}/${categoryId}`);
  }


}
