import { Injectable } from '@angular/core';
import { Categorie } from 'src/app/member/models/categorie';
import { CategoriesService } from './categories.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, map, mergeMap, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesResolveService implements Resolve<Categorie>{

  constructor(
    private categoriesService: CategoriesService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Categorie | Observable<Categorie> | Promise<Categorie> {
    const id = route.params['id'];
    if(id){
      return this.categoriesService.findAll({'id': id}).pipe(map(categories => categories[0]), mergeMap((categorie: Categorie) => {
        if(categorie){
          return of(categorie);
        } else {
          return EMPTY;
        }
      }));
    }
    return of({});
  }
}
