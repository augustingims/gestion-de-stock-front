import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Categorie } from 'src/app/member/models/categorie';
import { CategoriesService } from '../service/categories.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriesDeleteComponent } from '../categories-delete/categories-delete.component';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  categories$: Observable<Categorie[] | []> | undefined;

  constructor(
    private router: Router,
    private categoriesService: CategoriesService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.categories$ = this.categoriesService.findAll();
  }

  addCategory(): void {
    this.router.navigate(['categories', 'new']);
  }

  refresh(): void {
    this.categories$ = this.categoriesService.findAll();
  }

  trackById(_index: number, item: Categorie): number {
    return item.id!;
  }

  deleteCategory(categorie: Categorie): void {
    const deleteModal = this.modalService.open(CategoriesDeleteComponent, {backdrop: 'static'});
    deleteModal.componentInstance.categorie = categorie;
    deleteModal.closed.subscribe(reason => {
      if (reason === 'success') {
        this.refresh();
      }
    })
  };

}
