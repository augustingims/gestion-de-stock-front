import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Categorie } from 'src/app/member/models/categorie';
import { Utilisateur } from 'src/app/member/models/utilisateur';
import { CategoriesService } from '../service/categories.service';
import { AccountService } from 'src/app/core/auth/account.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categories-create-update',
  templateUrl: './categories-create-update.component.html',
  styleUrls: ['./categories-create-update.component.scss']
})
export class CategoriesCreateUpdateComponent implements OnInit, OnDestroy {

  account: Utilisateur | null = {}
  authSubscription: Subscription = new Subscription();
  categorie: Categorie | undefined;
  errorMsg: Array<string> = [];

  categoryForm = this.fb.group({
    code: [''],
    designation: ['']
  });

  constructor(
    private fb: FormBuilder,
    private categoriesService: CategoriesService,
    private accountService: AccountService,
    private activateRoute: ActivatedRoute
  ) { }

  ngOnDestroy(): void {
   this.authSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.authSubscription.add(this.accountService.getAuthenticationState().subscribe(account => (this.account = account)));
    this.activateRoute.data.subscribe(({ categorie }) => {
      this.categorie = categorie;
      this.updateForm(categorie);
    });
  }

  cancel(): void {
    window.history.back();
  }

  create(){
    const categorie = this.createForm();
    this.categoriesService.createCategory(categorie).subscribe({next: res => {
      this.cancel();
    }, error: error => {
      if(error.error != null && error.error.errors !== undefined ){
        this.errorMsg = error.error.errors;
      }
    }});
  }

  private updateForm(categorie?: Categorie): void {
    this.categoryForm.patchValue({
      code: categorie?.code,
      designation: categorie?.designation
    })
  }

  private createForm(): Categorie{
    return {
      ...this.categorie,
      code: this.categoryForm.get('code')?.value ?? '',
      designation: this.categoryForm.get('designation')?.value ?? '',
      idEntreprise: this.account?.entreprise?.id,
    }
  }

}
