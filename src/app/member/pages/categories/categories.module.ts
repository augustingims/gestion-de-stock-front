import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CategoriesCreateUpdateComponent } from './categories-create-update/categories-create-update.component';
import { CategoriesDeleteComponent } from './categories-delete/categories-delete.component';
import { SharedMemberModule } from '../../shared-member/shared-member.module';


@NgModule({
  declarations: [
    CategoriesListComponent,
    CategoriesCreateUpdateComponent,
    CategoriesDeleteComponent
  ],
  imports: [
    SharedMemberModule,
    CategoriesRoutingModule
  ],
  entryComponents: [
    CategoriesDeleteComponent
  ]
})
export class CategoriesModule { }
