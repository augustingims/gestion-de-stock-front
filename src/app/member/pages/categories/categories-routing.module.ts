import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CategoriesCreateUpdateComponent } from './categories-create-update/categories-create-update.component';
import { CategoriesResolveService } from './service/categories-resolve.service';

const routes: Routes = [
  {
    path: '',
    data: {
      seo : {
        title: "List des categories | Gestion Stock",
        description: "Page de la liste des categories ",
      }
    },
    component: CategoriesListComponent
  },
  {
    path: 'new',
    data: {
      seo : {
        title: "Creation categorie | Gestion Stock",
        description: "Page de creation d'une categorie ",
      }
    },
    component: CategoriesCreateUpdateComponent
  },
  {
    path: ':id/edit',
    data: {
      seo : {
        title: "Modification categorie | Gestion Stock",
        description: "Page de modification d'une categorie",
      }
    },
    component: CategoriesCreateUpdateComponent,
    resolve: {
      categorie: CategoriesResolveService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
