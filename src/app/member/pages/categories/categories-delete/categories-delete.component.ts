import { Component } from '@angular/core';
import { Categorie } from '../../../models/categorie';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriesService } from '../service/categories.service';

@Component({
  selector: 'app-categories-delete',
  templateUrl: './categories-delete.component.html',
  styleUrls: ['./categories-delete.component.scss']
})
export class CategoriesDeleteComponent {

  categorie!: Categorie ;
  isVisible = false;
  errorMsg: string = "";

  constructor(
    private categoriesService: CategoriesService,
    private activeModal: NgbActiveModal
  ) { }

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirm(id: number): void {
    this.isVisible = true;
    this.categoriesService.deleteCategories(id).subscribe({next: () => {
      this.activeModal.close('success');
      this.isVisible = false;
    }, error: err => {
      this.errorMsg = err.error.error;
      this.isVisible = false;
    }});
  }

}
