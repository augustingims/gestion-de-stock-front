import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button-actions',
  templateUrl: './button-actions.component.html',
  styleUrls: ['./button-actions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonActionsComponent {

  @Input() isNewShow = true;
  @Input() isExportShow = false;
  @Input() isImportShow = false;

  @Output() newEvent = new EventEmitter();

  btnNew(): void {
    this.newEvent.emit();
  }

}
