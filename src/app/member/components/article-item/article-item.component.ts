import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Article } from '../../models/article';
import { ArticlesDeleteComponent } from '../../pages/articles/articles-delete/articles-delete.component';

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush 
})
export class ArticleItemComponent {

  @Input()
  article: Article = {};

  @Output() deleteArticle = new EventEmitter();

  @Output() updateArticle = new EventEmitter();

  constructor(
    private router: Router,
    private modalSerive: NgbModal
  ) { }

  update(): void {
    this.router.navigate(['articles', this.article.id, 'edit']);
  }

  delete(): void{
    const deleteModal = this.modalSerive.open(ArticlesDeleteComponent, {backdrop: 'static'});
    deleteModal.componentInstance.article = this.article;
    deleteModal.closed.subscribe(reason => {
      if(reason === 'success'){
        this.deleteArticle.emit('article-delete');
      }
    });
  }

}
