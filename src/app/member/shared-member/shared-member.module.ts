import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ButtonActionsComponent } from '../components/button-actions/button-actions.component';
import { NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JsonLdModule } from 'ngx-seo';



@NgModule({
  declarations: [
    ButtonActionsComponent
  ],
  imports: [
    NgbModule,
    NgbAlertModule,
    SharedModule,
    JsonLdModule
  ],
  exports: [
    SharedModule,
    NgbModule,
    NgbAlertModule,
    ButtonActionsComponent,
    JsonLdModule
  ]
})
export class SharedMemberModule { }
