import { Sidebar } from "../../layouts/sidebar/sidebar.model";

export const menuProperties: Array<Sidebar> = [
    {
        id: '1',
        title: 'Tableau de bord',
        icon: 'fas fa-chart-line',
        url: '',
        active: true,
        subMenu: [
            {
                id: '11',
                title: 'Vue d\'ensemble',
                icon: 'fas fa-chart-pie',
                url: '',
                active: true
            },
            {
                id: '12',
                title: 'Statistiques',
                icon: 'fas fa-chart-line',
                url: 'statistiques',
                active: true
            }
        ]
    },
    {
        id: '2',
        title: 'Articles',
        icon: 'fas fa-boxes',
        url: '',
        active: false,
        subMenu: [
            {
                id: '21',
                title: 'Articles',
                icon: 'fas fa-chart-pie',
                url: 'articles',
                active: true
            },
            {
                id: '22',
                title: 'Mouvement de stock',
                icon: 'fas fa-boxes',
                url: 'mvtstk',
                active: true
            }
        ]
    },
    {
        id: '3',
        title: 'Clients',
        icon: 'fas fa-users',
        url: '',
        active: false,
        subMenu: [
            {
                id: '31',
                title: 'Clients',
                icon: 'fas fa-users',
                url: 'clients',
                active: true
            },
            {
                id: '32',
                title: 'Commandes clients',
                icon: 'fas fa-shopping-basket',
                url: 'commandeclient',
                active: true
            }
        ]
    },
    {
        id: '4',
        title: 'Fournisseurs',
        icon: 'fas fa-users',
        url: '',
        active: false,
        subMenu: [
            {
                id: '41',
                title: 'Fournisseurs',
                icon: 'fas fa-users',
                url: 'fournisseurs',
                active: true
            },
            {
                id: '42',
                title: 'Commandes fournisseurs',
                icon: 'fas fa-truck',
                url: 'commandefournisseur',
                active: true
            }
        ]
    }
    ,
    {
        id: '5',
        title: 'Parametrages',
        icon: 'fas fa-cogs',
        url: '',
        active: false,
        subMenu: [
            {
                id: '51',
                title: 'Categories',
                icon: 'fas fa-tools',
                url: 'categories',
                active: true
            },
            {
                id: '52',
                title: 'Utilisateurs',
                icon: 'fas fa-users-cog',
                url: 'utilisateurs',
                active: true
            }
        ]
    }

]