import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MemberRoutingModule } from './member-routing.module';
import { MemberComponent } from './member.component';
import { LayoutsModule } from './layouts/layouts.module';
import { SharedMemberModule } from './shared-member/shared-member.module';
@NgModule({
  imports: [
    CommonModule,
    LayoutsModule,
    SharedMemberModule,
    MemberRoutingModule
  ],
  declarations: [
    MemberComponent
  ]
})
export class MemberModule { }
