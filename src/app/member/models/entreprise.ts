/* tslint:disable */
import { Adresse } from './adresse';
export interface Entreprise {
  adresse?: Adresse;
  codeFiscal?: string;
  description?: string;
  email?: string;
  id?: number;
  nom?: string;
  numTel?: string;
  password?: string;
  photo?: string;
  steWeb?: string;
}
