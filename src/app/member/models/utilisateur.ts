/* tslint:disable */
import { Adresse } from './adresse';
import { Entreprise } from './entreprise';
import { Roles } from './roles';
export interface Utilisateur {
  adresse?: Adresse;
  dateDeNaissance?: number;
  email?: string;
  entreprise?: Entreprise;
  id?: number;
  moteDePasse?: string;
  nom?: string;
  photo?: string;
  prenom?: string;
  roles?: Array<Roles>;
}
