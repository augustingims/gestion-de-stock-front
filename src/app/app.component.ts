import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRouteSnapshot, NavigationEnd, Router, RoutesRecognized } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { Subscription, filter, pairwise } from 'rxjs';
import { title } from 'process';
import { WebStorageService } from './core/auth/web-storage.service';
import { event } from 'cypress/types/jquery';

@Component({
  selector: 'app-root',
  template: `
  <ngx-loading-bar></ngx-loading-bar>
  <router-outlet></router-outlet> 
  `,
  styleUrls: []
})
export class AppComponent implements OnInit {

  title = 'gestiondestock';

  serviceSubscription: Subscription = new Subscription();

  constructor(
    private swUpdate: SwUpdate,
    private router: Router,
    private titleService: Title,
    private metaService: Meta,
    private webStorageService: WebStorageService,
  ){
    router.events
   .pipe(filter((event: any) => event instanceof RoutesRecognized), pairwise())
   .subscribe((events: RoutesRecognized[]) => {
     if(events[1].urlAfterRedirects === '/login' || events[1].urlAfterRedirects === '/register' || events[1].urlAfterRedirects === '/reset-password') {
      if(events[0].urlAfterRedirects === '/login' || events[0].urlAfterRedirects === '/register' || events[0].urlAfterRedirects === '/reset-password') {
        this.webStorageService.storeUrl('/');
      } else {
        this.webStorageService.storeUrl(events[0].urlAfterRedirects);
      }
     } else if(events[0].urlAfterRedirects === '/login' || events[0].urlAfterRedirects === '/register' || events[0].urlAfterRedirects === '/reset-password') {
      if(events[1].urlAfterRedirects === '/login' || events[1].urlAfterRedirects === '/register' || events[1].urlAfterRedirects === '/reset-password') {
        this.webStorageService.storeUrl('/');
      } else {
        this.webStorageService.storeUrl(events[1].urlAfterRedirects);
      }
     } else {
      this.webStorageService.storeUrl(events[1].urlAfterRedirects);
    }
   });

  }

  ngOnInit(): void {
    if(this.swUpdate.isEnabled){
      this.swUpdate.available.subscribe(() => {
        this.swUpdate.activateUpdate().then(() => document.location.reload());
      });
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.updateTitle();
        this.updateDescription();
      }
    });

    // this.router.events.subscribe((event: any) => {
    //   if (event instanceof RoutesRecognized) {
    //     window.console.log(event);
    //   }
    // });

  }
  
  updateDescription(): void {
    let description = this.getDescription(this.router.routerState.snapshot.root);
    this.metaService.removeTag("name='description'");
    this.metaService.addTag({name: 'description', content: description});
  }

  updateTitle(): void {
    let title = this.getTitle(this.router.routerState.snapshot.root);
    this.titleService.setTitle(title);
  }

  getTitle(routeSnapshot: ActivatedRouteSnapshot): string {
    let title = routeSnapshot.data && routeSnapshot.data['seo'] ? routeSnapshot.data['seo'].title : '';
    if(routeSnapshot.firstChild){
      title = this.getTitle(routeSnapshot.firstChild) || title;
    }
    return title;
  }

  getDescription(routeSnapshot: ActivatedRouteSnapshot): string {
    let description = routeSnapshot.data && routeSnapshot.data['seo'] ? routeSnapshot.data['seo'].description : '';
    if(routeSnapshot.firstChild){
      description = this.getDescription(routeSnapshot.firstChild) || description;
    }
    return description;
  }

}
