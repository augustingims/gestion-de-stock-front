import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationConfigService {

  private endpointPrefix = environment.apiUrl;

  getEndpointFor(url: string): string {
    return `${this.endpointPrefix}/${url}`;
  }

}
