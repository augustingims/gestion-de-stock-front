import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebStorageService } from '../auth/web-storage.service';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private webStorageService: WebStorageService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string | null = this.webStorageService.getToken();
    if(token){
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        }
      })
    }
    return next.handle(req);
  }

}
