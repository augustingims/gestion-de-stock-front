import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { AuthJwtService } from '../auth/auth-jwt.service';

@Injectable()
export class AuthExpiredInterceptor implements HttpInterceptor {

  constructor(
    private authJwtService: AuthJwtService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap({
      error: (err: HttpErrorResponse) => {
        if([401,403,0].includes(err.status) && err.url && !err.url.includes("api/v1/utilisateurs/account")){
          this.authJwtService.logoutWithoutObserver();
        }
      }
    }));
  }

}
