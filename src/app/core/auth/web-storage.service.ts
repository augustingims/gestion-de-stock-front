import { Injectable } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class WebStorageService {

  protected tokenKey = 'access_token';
  protected previousUrlKey = 'previousUrl';

  constructor(
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService
  ) { }

  storeToken(token: string, rememberMe: boolean | null): void {
    if(rememberMe){
      this.$localStorage.store(this.tokenKey, token);
      this.$sessionStorage.clear(this.tokenKey);
    } else { 
      this.$sessionStorage.store(this.tokenKey, token);
      this.$localStorage.clear(this.tokenKey);
    }
  }

  storeUrl(url: string): void {
      this.$sessionStorage.store(this.previousUrlKey, url);
  }

  getToken(): string {
    if(this.$sessionStorage.retrieve(this.tokenKey)){
      return this.$sessionStorage.retrieve(this.tokenKey);
    } else {
      return this.$localStorage.retrieve(this.tokenKey);
    }
  }

  getUrl(): string | null {
    return this.$sessionStorage.retrieve(this.previousUrlKey) as string | null;
  }

  clearToken(): void {
     this.$sessionStorage.clear(this.tokenKey);
     this.$localStorage.clear(this.tokenKey);
  }

  clearUrl(): void {
    this.$sessionStorage.clear(this.previousUrlKey);
 }

}
