import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, map } from 'rxjs';
import { AuthenticationRequest } from './models/authentication-request';
import { AuthenticationResponse } from './models/authentication-response';
import { WebStorageService } from './web-storage.service';
import { Router } from '@angular/router';
import { ApplicationConfigService } from '../config/application-config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthJwtService {

  protected resourceUrl = "api/v1/authenticate";

constructor(
  private httpClient: HttpClient,
  private router: Router,
  private webStorageService: WebStorageService,
  private applicationConfigService: ApplicationConfigService
) { }

  login(credentials: AuthenticationRequest): Observable<void> {
    return this.httpClient.post(this.applicationConfigService.getEndpointFor(this.resourceUrl), credentials).pipe(map(response => this.authenticateSuccess(response, credentials.rememberMe)));
  }

  authenticateSuccess(response: AuthenticationResponse, rememberMe: boolean | null): void {
    const jwt = response.accessToken;
    this.webStorageService.storeToken(jwt!, rememberMe);
  }

  getToken(): string {
    return this.webStorageService.getToken();
  }

  logout(): Observable<void>{
    return new Observable<void>(observer => {
      this.webStorageService.clearToken();
      observer.complete();
    });
  }

  isAuthentication(): boolean {
    if(this.getToken()){
      return true;
    }
    this.router.navigate(['login']);
    return false;
  }

  logoutWithoutObserver(): void{
    this.webStorageService.clearToken();
    this.router.navigate(['login']);
  }

}
