import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, catchError, of, shareReplay, tap } from 'rxjs';
import { Utilisateur } from 'src/app/member/models/utilisateur';
import { UtilisateurService } from 'src/app/member/pages/utilisateur/utilisateur.service';
import { WebStorageService } from './web-storage.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private userIdentity: Utilisateur | null = null;
  private authenticationState = new ReplaySubject<Utilisateur | null>(1);
  private accountCache$?: Observable<Utilisateur> | null;

  constructor(
    private utilisateurService: UtilisateurService,
    private webStorageService: WebStorageService,
    private router: Router
  ) { }

  getCurrentUser(): Observable<Utilisateur> {
    return this.utilisateurService.currentUser();
  }

  identify(force?: boolean): Observable<Utilisateur | null> {
    if(!this.accountCache$ || force ){
      this.accountCache$ = this.getCurrentUser().pipe(tap((account: Utilisateur) => {
        this.authentication(account);
      }),
      shareReplay()
    );
    }
    return this.accountCache$.pipe(catchError(() => of(null)));
  }
  navigateToStoreUrl(): void {
   const returnUrl = this.webStorageService.getUrl();
   if(returnUrl){
    this.webStorageService.clearUrl();
    this.router.navigateByUrl(returnUrl);
   }
  }

  authentication(account: Utilisateur | null): void {
   this.userIdentity = account;
   this.authenticationState.next(account);
   if(account){
    this.accountCache$ = null;
   }
  }

  isAuthenticated(): boolean {
   return this.userIdentity !== null
  }

  getAuthenticationState(): Observable<Utilisateur | null> {
    return this.authenticationState.asObservable();
  }


}
