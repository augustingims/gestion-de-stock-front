/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserRouterAccessService } from './user-router-access.service';

describe('Service: UserRouterAccess', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserRouterAccessService]
    });
  });

  it('should ...', inject([UserRouterAccessService], (service: UserRouterAccessService) => {
    expect(service).toBeTruthy();
  }));
});
