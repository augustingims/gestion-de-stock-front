/* tslint:disable */
export interface AuthenticationRequest {
  login: string | null;
  password: string | null;
  rememberMe: boolean | null;
}
