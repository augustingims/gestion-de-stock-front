import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, map } from 'rxjs';
import { AccountService } from './account.service';
import { AuthJwtService } from './auth-jwt.service';

@Injectable({
  providedIn: 'root'
})
export class UserRouterAccessService implements CanActivate {

  constructor(
    private router: Router,
    private authJwtService: AuthJwtService,
    private accountService: AccountService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
   //return this.authJwtService.isAuthentication();
   return this.accountService.identify().pipe(map(account => {
    if(account){
      return true;
    }
    this.router.navigate(['login']);
    return false;
   }));
  }

}
