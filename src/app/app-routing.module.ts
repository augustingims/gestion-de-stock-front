import { NgModule } from '@angular/core';
import { NoPreloading, PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { MemberComponent } from './member/member.component';
import { UserRouterAccessService } from './core/auth/user-router-access.service';
import { QuicklinkStrategy } from 'ngx-quicklink';

const routes: Routes = [
  {
    path: '',
    component: MemberComponent,
    canActivate: [UserRouterAccessService],
    loadChildren: () => import("./member/member.module").then(m => m.MemberModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking',
    preloadingStrategy: QuicklinkStrategy
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
