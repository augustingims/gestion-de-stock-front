import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthJwtService } from 'src/app/core/auth/auth-jwt.service';
import { AuthenticationRequest } from 'src/app/core/auth/models/authentication-request';
import { WebStorageService } from 'src/app/core/auth/web-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMessage: string | undefined;

  loginForm = this.fb.group({
    login: [null, [Validators.required]],
    password: [null, [Validators.required]],
    rememberMe: [false]
  });
  
  constructor(
    private authJwtService: AuthJwtService,
    private webStorageService: WebStorageService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  login(): void {

    const loginRequest: AuthenticationRequest = {
      login: this.loginForm.get("login")!.value,
      password: this.loginForm.get("password")!.value,
      rememberMe: this.loginForm.get("rememberMe")!.value,
    };

    this.authJwtService.login(loginRequest).subscribe({next: () => {
      this.navigateToStoreUrl();
    }, error: error => {
      this.errorMessage = error.error?.message;
    }})
  }

  navigateToStoreUrl(): void {
    const returnUrl = this.webStorageService.getUrl();
    if(returnUrl){
     this.webStorageService.clearUrl();
     this.router.navigateByUrl(returnUrl);
    }
   }

}
