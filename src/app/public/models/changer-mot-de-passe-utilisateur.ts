/* tslint:disable */
export interface ChangerMotDePasseUtilisateur {
  confirmMotDePasse?: string;
  id?: number;
  motDePasse?: string;
}
