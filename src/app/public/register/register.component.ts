import { Component, OnInit } from '@angular/core';
import { Adresse } from 'src/app/member/models/adresse';
import { Entreprise } from 'src/app/member/models/entreprise';
import { AuthJwtService } from '../../core/auth/auth-jwt.service';
import { EntrepriseService } from 'src/app/member/pages/entreprise/entreprise.service';
import { Router } from '@angular/router';
import { AuthenticationRequest } from 'src/app/core/auth/models/authentication-request';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  entreprise: Entreprise = {};
  adresse: Adresse = {};
  errorMsg: Array<string> = [];
  isSaving = false;

  constructor(
    private router: Router,
    private entrepriseService: EntrepriseService,
    private authJwtService: AuthJwtService
  ) { }

  ngOnInit(): void {
  }

  register():void {
    this.isSaving = true;
    this.entreprise.adresse = this.adresse;
    this.entrepriseService.createEntreprise(this.entreprise).subscribe({next: () => {
      this.isSaving = false;
      this.connectUser();
    }, error: error => {
      if(error.error != null && error.error.errors !== undefined ){
        this.errorMsg = error.error.errors;
      }
      this.isSaving = false;
    }})
  }

  connectUser(): void {
    const loginRequest: AuthenticationRequest = {
      login: this.entreprise.email!,
      password: this.entreprise.password!,
      rememberMe: false,
    };

    this.authJwtService.login(loginRequest).subscribe({next: () => {
      this.router.navigate(['']);
    }, error: () => {
      this.router.navigate(['login']);
    }});
  }

}
