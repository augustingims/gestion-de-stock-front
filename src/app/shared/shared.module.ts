import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { ToastrModule } from 'ngx-toastr';
import { QuicklinkModule } from 'ngx-quicklink';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    QuicklinkModule,
    ToastrModule.forRoot(),
    LoadingBarHttpClientModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    QuicklinkModule,
    ToastrModule,
    LoadingBarHttpClientModule
  ]
})
export class SharedModule { }
